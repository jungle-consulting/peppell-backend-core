package entity

import "gorm.io/gorm"

type Point struct {
	gorm.Model
	ID          uint
	UserID      uint
	Longitude   float64 `gorm:"type:decimal(14, 9)"`
	Latitude    float64 `gorm:"type:decimal(14, 9)"`
	PointImages []PointImage
	CityID      *uint  `gorm:"type:bigint(20);index"`
	City        *City  `gorm:"foreignkey:city_id;references:id"`
	Comment     string `gorm:"type:varchar(120)"`
}
